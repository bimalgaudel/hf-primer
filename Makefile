CXX = g++
CXXFLAGS = -Wall -g
LD_FLAGS = -g

main: main.o integrals.o matrices.o
	$(CXX) $(CXXFLAGS) -o hartree-fock main.o integrals.o matrices.o
main.o: integrals.h matrices.h
	$(CXX) $(CXXFLAGS) -c main.cpp
matrices.o: integrals.h matrices.h
integrals.o: integrals.h
