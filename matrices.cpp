#include <vector>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include "integrals.h"
#include "matrices.h"

Eigen::MatrixXd _matrix_generic_olap_kind(const Molecule& molecule,
                                double integral_function(PrimPair pair))
{
    // given a molecule, applies the
    // integral_function on the matrix formed
    // by the basis of the primitives, the atoms
    // of the molecule possess
    // eg. If a, b, c, d primitives are present,
    //   a  b  c  d
    //   |  |  |  |
    // a-+--+--+--+
    //   |  |  |  |
    // b-+--+--+--+  + = result of integral_function(<pair>)
    //   |  |  |  |       e.g. the first '+' from top
    // c-+--+--+--+       left would be integral_function(a,a)
    //   |  |  |  |
    // d-+--+--+--+
    //
    // the matrix of all '+'s will be returned
    //
    Prims prims = molecule.prims;
    int size = prims.size();
    Eigen::MatrixXd matrix(size,size);
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            if (i > j) {
                matrix(i,j) = matrix(j,i);
                continue;
            }
            PrimPair pair = {prims[i], prims[j]};
            matrix(i,j) = integral_function(pair);
        }
    }
    return matrix;
}

Eigen::MatrixXd matrix_overlap(const Molecule& molecule)
{
    // calculates the integral overlap matrix of normed primitives
    return _matrix_generic_olap_kind(molecule, integral_overlap);
}

Eigen::MatrixXd matrix_ke(const Molecule& molecule)
{
    // calculates the kinetic energy matrix of the primitives
    return _matrix_generic_olap_kind(molecule, integral_ke);
}

Eigen::MatrixXd matrix_nuclear_att(const Molecule& molecule)
{
    // extract the primitives and the nuclei
    // from the atoms
    Prims prims = molecule.prims;
    Nuclei nuclei = molecule.nuclei;
    // the nuclear attraction matrix
    Eigen::MatrixXd matrix(prims.size(), prims.size());
    for (unsigned int i = 0; i < prims.size(); ++i) {
        for (unsigned int j = 0; j < prims.size(); ++j) {
            if (i > j) {
                matrix(i,j) = matrix(j,i);
                continue;
            }
            PrimPair pair = {prims[i], prims[j]};
            matrix(i,j) = integral_nuclear_att(nuclei, pair);
        }
    }
    return matrix;
}

Eigen::MatrixXd matrix_electric_rep(const Molecule& molecule)
{
    // collect all the primitives
    Prims prims = molecule.prims;
    Eigen::MatrixXd matrix(prims.size(), prims.size());
    //
    for (unsigned int i = 0; i < prims.size(); ++i) {
        for (unsigned int j = 0; j < prims.size(); ++j) {
            if (i > j) {
                matrix(i,j) = matrix(j,i);
                continue;
            }
            PrimPair pair = {prims[i], prims[j]};
            matrix(i,j) = integral_electric_rep(pair, pair);
        }
    }
    return matrix;
}

Eigen::MatrixXd matrix_hamiltonian_core(const Molecule& molecule)
{
    // the Hamiltonian matrix is the sum of the kinetic energy
    // matrix and the nuclear_attraction_potential mattrix
    return matrix_ke(molecule) + matrix_nuclear_att(molecule);
}

Eigen::MatrixXd matrix_density(const Molecule& molecule,
                                const Eigen::MatrixXd& eigen_vectors_mat) {
    // see equation 37 from the project
    int size = eigen_vectors_mat.rows(); // or .cols()
    int num_occ_orbs = molecule.num_occ_orbs;
    Eigen::MatrixXd matrix(size, size);
    for (int rho = 0; rho < size; ++rho) {
        for (int sigma = 0; sigma < size; ++sigma) {
            if (rho > sigma) {
                matrix(rho, sigma) = matrix(sigma, rho);
                continue;
            }
            double sum = 0;
            for (int i = 0; i < num_occ_orbs; ++i) {
                sum += eigen_vectors_mat(rho, i)
                        * eigen_vectors_mat(sigma,i);
            }
            matrix(rho, sigma) = sum;
        }
    }
    return matrix;
}

Eigen::MatrixXd matrix_coulomb(const Molecule& molecule,
                                const Eigen::MatrixXd& density_mat) {
    // see equation 35 from the project
    const Prims& prims = molecule.prims;
    int size = prims.size();
    //
    Eigen::MatrixXd matrix(size, size);
    //
    for (int mu = 0; mu < size; ++mu) {
        for (int neu = 0; neu < size; ++neu) {
            if (mu > neu) {
                matrix(mu, neu) = matrix(neu, mu);
                continue;
            }
            double sum = 0;
            for (int rho = 0; rho < size; ++rho) {
                for (int sigma = 0; sigma < size; ++sigma) {
                    PrimPair pair_a = {prims[mu], prims[neu]};
                    PrimPair pair_b = {prims[rho], prims[sigma]};
                    sum += integral_electric_rep(pair_a, pair_b)
                            * density_mat(rho, sigma);
                }
            }
            matrix(mu, neu) = sum;
        }
    }
    return matrix;
}

Eigen::MatrixXd matrix_exchange(const Molecule& molecule,
                                 const Eigen::MatrixXd& density_mat) {
    // see equation 36 from the project
    // vector of bases
    const Prims& prims = molecule.prims;
    int size = prims.size();
    //
    Eigen::MatrixXd matrix(size, size);
    //
    for (int mu = 0; mu < size; ++mu) {
        for (int neu = 0; neu < size; ++neu) {
            if (mu > neu) {
                matrix(mu, neu) = matrix(neu, mu);
                continue;
            }
            double sum = 0;
            for (int rho = 0; rho < size; ++rho) {
                for (int sigma = 0; sigma < size; ++sigma) {
                    PrimPair pair_a = {prims[mu], prims[sigma]};
                    PrimPair pair_b = {prims[rho], prims[neu]};
                    sum += integral_electric_rep(pair_a, pair_b)
                            * density_mat(rho, sigma);
                }
            }
            matrix(mu, neu) = sum;
        }
    }
    return matrix;
}

Eigen::VectorXd gs_energy_1e(const Eigen::MatrixXd& hamil_mat,
                              const Eigen::MatrixXd& overlap_mat) {
    //
    // just to solve the problem 7 of the project
    //
    Eigen::GeneralizedSelfAdjointEigenSolver<Eigen::MatrixXd>
        es(hamil_mat, overlap_mat);
    return es.eigenvalues();
}

double energy_electronic_hf(const Eigen::MatrixXd& core_hamil_mat,
                             const Eigen::MatrixXd& fock_mat,
                             const Eigen::MatrixXd& density_mat) {
    Eigen::MatrixXd mat_A = core_hamil_mat + fock_mat;
    Eigen::MatrixXd mat_B = density_mat;
    double product_sum    = mat_A.cwiseProduct(mat_B).sum();
    /* return 1.0/2*product_sum; */
    return product_sum;
}

double energy_hartree_fock(const Molecule& molecule,
                            const Eigen::MatrixXd& core_hamil_mat,
                            const Eigen::MatrixXd& fock_mat,
                            const Eigen::MatrixXd& density_mat) {
    return energy_electronic_hf(core_hamil_mat, fock_mat, density_mat)
            + energy_nuclear(molecule);
}
