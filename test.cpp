#include <iostream>
#include <fstream>
#include "integrals.h"
#include "matrices.h"
#include "test.h"

using std::cout;
using std::endl;

void test(Molecule mol)
{
    cout << endl << endl;
    cout << "Calculating for the provided primitives\n";
    cout << "***************************************\n\n";
    cout << "Calculating overlap matrix\n";
    cout << matrix_overlap(mol) << endl << endl;
    //
    cout << "Calculating kinetic energy matrix\n";
    cout << matrix_ke(mol) << endl << endl;
    //
    cout << "Calculating nuclear attraction energy matrix\n";
    cout << matrix_nuclear_att(mol) << endl << endl;
    //
    cout << "Calculating electron repulsion matrix\n";
    cout << matrix_electric_rep(mol) << endl << endl;
    //
    cout << "Calculating core hamiltonian matrix\n";
    cout << matrix_hamiltonian_core(mol) << endl << endl;
}

