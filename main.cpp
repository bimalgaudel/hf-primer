#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "integrals.h"
#include "matrices.h"
#include "test.h"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;

// helper functions
bool BothAreSpaces(char lhs, char rhs);
bool parse_atoms(unsigned int pos, const vector<vector<string>>& vec,
                    unsigned int& charge_1, unsigned int& charge_2);
bool parse_basis(unsigned int pos, const vector<vector<string>>& vec,
                  vector<double>& alphas);
bool parse_cbas(unsigned int pos, const vector<vector<string>>& vec,
                 vector<double>& alphas);
bool parse_distance(unsigned int pos,
       const vector<vector<string>>& vec, double& distance);
//
bool check_input(vector<vector<string>>& parsed_lines);
bool fill_values(vector<vector<string>>& parsed_lines,
        Molecule& molecule);
vector<vector<string>> parse_file(string filename);
Molecule make_molecule(vector<vector<string>>& parsed_lines);

void hf_scf(std::ofstream& outfile, Molecule mol);
void schro_1e_1atom(std::ofstream& outfile, Molecule mol);

// 
int main(int argc, char *argv[])
{
    if ( argc != 2 )
    {
        cerr << "Usage: " << argv[0] << " <input.file>\n";
        return 1;
    } 
     
    //
    string fullname = argv[1];
    string rawname = fullname.substr(0, fullname.find_last_of("."));
     
    //
    std::ofstream outfile;
    outfile.open(rawname+".out");

    vector<vector<string>> parsed_lines = parse_file(fullname);
    if (check_input(parsed_lines)) {
        Molecule mol = make_molecule(parsed_lines);
        if (mol.num_occ_orbs > 0 && mol.num_occ_orbs < 3) { // only hydrogen and helium supported
            outfile << "**********************************\n";
            outfile << "    Calculations on " << fullname << endl;
            outfile << "**********************************\n";
            if (mol.atoms.size() == 1) {
                schro_1e_1atom(outfile, mol);}
            else hf_scf(outfile, mol);
            outfile << "\n        End of calculations\n";
            outfile << "**********************************\n";
            return 0;
        }
        cerr << "Only hydrogen and helium supported\n";
        return 1;
    }
    return 1;
    // test(Molecule mol);      // verifies solutions to the problems 1 through 7
    // schro_1e_1atom(Molecule mol); // solves Schrodinger equation for hydrogen atom
}

void hf_scf(std::ofstream& outfile, Molecule mol)
{
    // solve the canonical HF equations
    //       Fc = ScE
    // self-consistently for the given Molecule
    //
    // takes two more parameters:
    // max_iteration is 20 by default
    // min_norm is 10^-6 by default
    // 
    // min_norm is the Forbenius norm of
    // the difference between two density 
    // matrices from consecutive iterations
    const Eigen::MatrixXd overlap_mat = matrix_overlap(mol);
    const Eigen::MatrixXd core_hamil_mat   = matrix_hamiltonian_core(mol);

    // the density matrix is zero to start with
    const int size = mol.prims.size();
    Eigen::MatrixXd density_mat = Eigen::MatrixXd::Zero(size, size);

    // and the Fock matrix is just the core Hamiltonian
    Eigen::MatrixXd fock_mat = core_hamil_mat;

    // prepare to loop
    Eigen::MatrixXd counter_mat    = density_mat;
    unsigned int max_iteration     = mol.max_iteration;
    double       min_norm          = mol.min_norm;
    unsigned int counter_iteration = 1;
    double       forbenius_norm    = 1000; // initialize something surely higher than min_norm

    while ((counter_iteration < max_iteration) && (forbenius_norm > min_norm)) {
        //
        outfile << "\nIteration " << counter_iteration << endl;
        outfile << "----------\n";
        // calculate the c matrix
        // using the eigen solver
        Eigen::GeneralizedSelfAdjointEigenSolver<Eigen::MatrixXd> es;
        es.compute(fock_mat, overlap_mat);
        // update density matrix
        density_mat = matrix_density(mol, es.eigenvectors());
        //
        // update the Fock matrix
        fock_mat = core_hamil_mat
            + 2*matrix_coulomb(mol, density_mat)
            - matrix_exchange(mol, density_mat);
        //
        outfile << "\nThe Hartree-Fock energy is: ";
        outfile << energy_hartree_fock(mol, core_hamil_mat,
                fock_mat, density_mat);
        outfile << endl;
        //
        // handle the loop breaking criteria
        forbenius_norm = (density_mat - counter_mat).norm();
        counter_mat = density_mat;
        ++counter_iteration;
    }
    outfile << "Convergence reached after " << counter_iteration-1 << " cycles\n";
    if (counter_iteration == max_iteration-1) {
        outfile << "You might want to use stricter convergence criteria.";
    }
}

void schro_1e_1atom(std::ofstream& outfile, Molecule mol)
{
    //
    const Eigen::MatrixXd hamil_mat = matrix_hamiltonian_core(mol);
    const Eigen::MatrixXd overlap_mat= matrix_overlap(mol);
    //
    outfile << "\n\nThe Hamiltonian matrix is\n";
    outfile << "*************************\n\n";
    outfile << hamil_mat << endl << endl;
    //
    outfile << "\n\nThe Overlap matrix is\n";
    outfile << "*********************\n\n";
    outfile << overlap_mat << endl << endl;
    //
    outfile << "The ground state energy is\n";
    outfile << "**************************\n\n";
    outfile << gs_energy_1e(hamil_mat, overlap_mat)[0] << endl;
}

vector<vector<string>> parse_file(string filename)
{
    std::ifstream inpfile;
    inpfile.open(filename);
    vector<vector<string>> parsed_lines;

    //
    while (!inpfile.eof())
    {
        string line;
        std::getline(inpfile, line);

        if (line.size() == 0) {continue;}

        // let's truncate the repeating whitespace
        // to single whitespace
        string::iterator new_end = std::unique(line.begin(),
                line.end(),
                BothAreSpaces);
        line.erase(new_end, line.end());
        std::transform(line.begin(), line.end(), line.begin(), ::tolower);

        // parse the goodies in the now good line
        vector<string> parsed_words;
        std::istringstream iss(line);
        string tmp;
        char delim = ' ';
        while (std::getline(iss, tmp, delim))
        {
            // anything beyond # on a line is a comment
            if (tmp[0] == '#') {break;}
            parsed_words.push_back(tmp);
        }
        if (! parsed_words.empty()) {
            parsed_lines.push_back(parsed_words);
        }
    }
    inpfile.close();
    return parsed_lines;
}

bool check_input(vector<vector<string>>& parsed_lines)
{
    int v = 0;
    vector<vector<string>>::const_iterator i;
    for (i = parsed_lines.begin(); i != parsed_lines.end(); ++i) {
        if ( (*i)[0] == "atoms" 
                || (*i)[0] == "distance"
           ) {
            v += 2; }
        else if ((*i)[0] == "basis"
                || (*i)[0] == "cbas") {
            ++v;
        }
    }
    // if v > 4: we got enough info to build a molecule
    if (! (v > 4)) {
        cerr << "Input file is incomplete!\n";
        return false;
    }
    return true;
}

bool fill_values(vector<vector<string>>& parsed_lines, Molecule& molecule)
{
    vector<string> parsed_headers;
    for (vector<vector<string>>::const_iterator i = parsed_lines.begin();
            i != parsed_lines.end(); ++i) {
        parsed_headers.push_back((*i)[0]);
    }

    // locate the headers
    unsigned int ATOMS = 0, DISTANCE = 0;
    for (unsigned int pos = 0; pos < parsed_headers.size(); ++pos) {
        string head = parsed_headers[pos];
        if      (head == "atoms")    ATOMS    = pos; 
        else if (head == "distance") DISTANCE = pos;
    }

    // did we get everything in order
    bool A_PARSE = 0, B_PARSE = 0, C_PARSE = 0, D_PARSE = 0;

    unsigned int charge_1 = 0, charge_2 = 0;
    A_PARSE = parse_atoms(ATOMS, parsed_lines, charge_1, charge_2);

    double distance = 0;
    D_PARSE = parse_distance(DISTANCE, parsed_lines, distance);

    vector<double> alphas{0.0};
    for (int i = parsed_headers.size()-1; i >= 0; --i) {
        if (parsed_headers[i] == "basis") {
            B_PARSE = parse_basis(i, parsed_lines, alphas);
            break;
        }
        else if (parsed_headers[i] == "cbas") {
            C_PARSE = parse_cbas(i, parsed_lines, alphas);
            break;
        }
        //
    }

    if (! (A_PARSE && D_PARSE && (B_PARSE || C_PARSE))) {
        cerr << "Error in input values!\n";
        return false;
    }

    // everything fine so far
    //
    // we are using s-type orbitals only
    const vector<int> Q_NUMS      = {0,0,0};
    //
    const vector<double> POS_1    = {0.0, 0.0, 0.0};
    //
    Nucleus nuc_1 = {charge_1, POS_1};

    Prims prims1;
    //
    for (unsigned int i = 0; i < alphas.size(); ++i) {
        Prim prim = {alphas[i], Q_NUMS, nuc_1};
        prims1.push_back(prim);
    }
    //
    Atom a1 = {"", nuc_1, prims1};

    // if we got just one atom, let's pack up
    if (charge_2 == 0) {
        molecule = Molecule({a1});
        return true;
    }

    //
    const vector<double> POS_2    = {distance, 0.0, 0.0};

    //
    Nucleus nuc_2 = {charge_2, POS_2};

    Prims prims2;
    //
    for (unsigned int i = 0; i < alphas.size(); ++i) {
        Prim prim = {alphas[i], Q_NUMS, nuc_2};
        prims2.push_back(prim);
    }
    // setting up the two atoms
    Atom a2 = {"", nuc_2, prims2};
    // finally the molecule
    Molecule mol({a1, a2});
    molecule = mol;
    return true;
}

Molecule make_molecule(vector<vector<string>>& parsed_lines)
{
    // creates a molecule from a perfect input
    // create a dummy molecule
    Nucleus nuc = {0, {0.0,0.0,0.0}};
    Prim prim = {0.0, {0,0,0}, nuc};
    Atom atm = {"", nuc, {prim}};
    Molecule mol = {{atm}};
    bool good_data = fill_values(parsed_lines, mol);
    if (! good_data) { mol.num_occ_orbs = 0;} // num_occ_orbs can be used as a checker
    return mol;
}

bool BothAreSpaces(char lhs, char rhs)
{
    // useful to truncate whitespace
    return (lhs == rhs) && (lhs == ' ');
} 

bool parse_atoms(unsigned int pos, const vector<vector<string>>& vec,
                    unsigned int& charge_1, unsigned int& charge_2)
{
    if (! (vec[pos][0] == "atoms" && vec[pos].size() > 1)) {
        cerr << "No molecule/atom detected!\n";
        return false;
    }
    else if (vec[pos].size() > MAX_ATOMS+1) {   // +1 because first entry is "atoms"
        cerr << "Can't handle so many atoms!\n";
        return false;
    }

    std::istringstream iss1(vec[pos][1]);
    if (vec[pos].size() == 2) { // got just an atom
        charge_2 = 0;
        return (! (iss1 >> charge_1).fail());
    }
    std::istringstream iss2(vec[pos][2]);
     
    // validating nuclear charge by str to int conversion
    return (! ((iss1 >> charge_1).fail() || (iss2 >> charge_2).fail() ));
}

bool parse_basis(unsigned int pos, const vector<vector<string>>& vec,
                  vector<double>& alphas)
{
    if (! (vec[pos][0] == "basis" && vec[pos].size() == 2)) {
        cerr << "Basis set error!";
        return false;
    }
    if (vec[pos][1] == "3-21g") { // || vec[pos][2] == "6-31g";
        alphas = {5.44717800, 0.82454724, 0.18319158};
        return true;
    }
    else {
        cerr << "Basis set not-recognized!\n";
        return false;
    }
}

bool parse_cbas(unsigned int pos, const vector<vector<string>>& vec,
                 vector<double>& alphas)
{
    if (! (vec[pos][0] == "cbas" && vec[pos].size() > 1)) {
        cerr << "Didn't get basis set!";
        return false;
    }
    vector<double> got_these_alphas;
    double i;
    vector<string>::const_iterator iter = vec[pos].begin();
    ++iter; // skip the header
    for (; iter != vec[pos].end(); ++iter) {
         
        std::istringstream iss(*iter);
        if ((iss >> i).fail()) {
            cerr << "Custom basis set specification error!\n";
            return false;
        }
        got_these_alphas.push_back(i);
    }
    alphas = got_these_alphas;
    return true;
}
 
bool parse_distance(unsigned int pos,
       const vector<vector<string>>& vec, double& distance)
{
    if (! (vec[pos][0] == "distance" && vec[pos].size() == 2)) {
        cerr << "Distance between two atoms is wrong!\n";
        return false;
    }
    std::istringstream iss(vec[pos][1]);
    return (! (iss >> distance).fail());
}
