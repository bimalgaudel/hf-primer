#ifndef MATRICES_H
#define MATRICES_H 
#include <Eigen/Eigen>
#include "integrals.h"

// most of the matrix functions below take
// Molecule& reference because, the Molecule
// struct has the data members: primitives, 
// nuclei, number of electrons, number of occupied
// orbitals etc. So, it's easy to pass the reference
// to the molecule rather than keeping track of which
// parameters to pass in what orders

Eigen::MatrixXd matrix_overlap(const Molecule& molecule);
Eigen::MatrixXd matrix_ke(const Molecule& molecule);
Eigen::MatrixXd matrix_nuclear_att(const Molecule& molecule);
Eigen::MatrixXd matrix_electric_rep(const Molecule& molecule);
Eigen::MatrixXd matrix_hamiltonian_core(const Molecule& molecule);
//
Eigen::MatrixXd matrix_density(const Molecule& molecule,
                                const Eigen::MatrixXd& eigen_vectors_mat);
                               
Eigen::MatrixXd matrix_coulomb(const Molecule& molecule,
                                const Eigen::MatrixXd& density_mat);

Eigen::MatrixXd matrix_exchange(const Molecule& molecule,
                                 const Eigen::MatrixXd& density_mat);

//
Eigen::VectorXd gs_energy_1e(const Eigen::MatrixXd& hamil_mat,
                              const Eigen::MatrixXd& overlap_mat);
//
double energy_electronic_hf(const Eigen::MatrixXd& core_hamil_mat,
                             const Eigen::MatrixXd& fock_mat,
                             const Eigen::MatrixXd& density_mat);
//
double energy_hartree_fock(const Molecule& molecule,
                            const Eigen::MatrixXd& core_hamil_mat,
                            const Eigen::MatrixXd& fock_mat,
                            const Eigen::MatrixXd& density_mat);
//*****************************
Eigen::MatrixXd _matrix_generic(const Molecule& molecule,
                                 double integral_function(PrimPair pair));
#endif /* ifndef MATRICES_H */
