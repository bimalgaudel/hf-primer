#ifndef INTEGRALS_H
#define INTEGRALS_H 
#include <vector>
#include <string>

// default convergence criteria
const unsigned int MAX_ITERATION = 20;
const double           MIN_NORM  = 1e-6;

// maximum number of atoms supported
const unsigned int MAX_ATOMS = 2;

// the three dimensions
enum {X, Y, Z, DIMS};

typedef std::vector<double> pos_vec;
typedef std::vector<int> qnum_vec;

struct Nucleus
{
    unsigned int charge;
    pos_vec position;
};
//
typedef std::vector<Nucleus> Nuclei;

struct Prim
{
    double alpha;
    qnum_vec q_nums;
    Nucleus nucleus;
    pos_vec position = nucleus.position;
};
//
typedef std::vector<Prim> Prims;

// useful while calculating electron pair integrals
struct PrimPair
{
    Prim prim_a;
    Prim prim_b;
};

struct Atom
{
    std::string symb;
    Nucleus nucleus;
    Prims prims;
    pos_vec position = nucleus.position;
};
//
typedef std::vector<Atom> Atoms;
//

struct Molecule
{
    Atoms atoms;
    Prims prims;
    Nuclei nuclei;
    int num_electrons = 0;
    unsigned int num_occ_orbs = 0;
    unsigned int num_atoms = 0;
    std::string fmla = "";
    // default convergence criteria
    unsigned int max_iteration = MAX_ITERATION;
    double min_norm = MIN_NORM;
     
    Molecule(Atoms ats){
        atoms = ats;
        num_atoms = atoms.size();
        for (unsigned int i = 0; i < atoms.size(); ++i) {
            prims.insert(prims.end(),
                    atoms[i].prims.begin(),
                    atoms[i].prims.end());
            nuclei.push_back(atoms[i].nucleus);
            fmla += atoms[i].symb + '-';
            num_electrons += atoms[i].nucleus.charge;
        }
        fmla.pop_back();
        num_occ_orbs = num_electrons/2 + (num_electrons%2 != 0);
    }
};

//
//****************************************************
// use these functions to calculate matrices

double integral_overlap(PrimPair pair);

double integral_ke(PrimPair pair);

double integral_nuclear_att(Nuclei nuclei, PrimPair pair);

double integral_electric_rep(PrimPair pair_x, PrimPair pair_y);

//****************************************************

double _integral_overlap_unormed(PrimPair pair);

double _integral_overlap_1d(PrimPair pair, int dim);

double _angular_momentum_function(int index,
                                int l_1, int l_2,
                                double L_1, double L_2);

//****************************************************

double norm_const_stype(double orb_exponent);

int factorial(int n, int stop=1);

int combination(int a, int b);

double distance_sq(pos_vec pos_a, pos_vec pos_b);

//
double energy_nuclear(Molecule molecule);

#endif /* ifndef INTEGRALS_H */
