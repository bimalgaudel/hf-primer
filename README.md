Restricted Closed Shell Hartree-Fock Calculation for Diatomic Bi-electronic Systems
-----------------------------------------------------------------------------------

This project is a primer to the electronic structure calculations.
See ```project.pdf``` in ```doc``` for problem statements.

*Thanks Dr. [Varun Rishi](https://github.com/varunrishi) and Mr. [Samuel Slattery](https://github.com/samslattery) for helping me find the bugs.*

Files
-----
All integral functions, structs, and functions that don't require Eigen/Eigen are defined in:
```
integrals.h
integrals.cpp
```
All matrix operations are defined in:
```
matrices.h
matrices.cpp
```


Data Structures
---------------
The following are structs.
```
Nucleus
Prim
Atom
Molecule
PrimPair
```

#### Nucleus
```
  position             pos_vec ( = std::vector<double> )
  charge               int 
```
#### Prim(Nucleus)
```
  alpha                double
  q_nums               qnum_vec ( = std::vector<int> )
  nucleus              Nucleus
  position             pos_vec
```
#### Atom(Prims, Nucleus)
```
  symb                 std::string
  prims                Prims ( = std::vector<Prim> )
  nucleus              Nucleus
  position             pos_vec
```
#### Molecule(Atoms)
```
  atoms                Atoms ( = std::vector<Atom> )
  prims                Prims
  nuclei               Nuclei ( = std::vector<Nucleus> )
  num_electrons        unsigned int
  num_occ_orbs         unsigned int
  fmla                 std::string
```
#### PrimPair(Pair)
```
prim_a                 Prim
prim_b                 Prim
```
### The dependency map
```



                             +-----------------+
                             |                 |
                             |                 |
                             |     Molecule    |
                             |                 |
                             |                 |
                      +----->+--------^--------+<------+
                      |               |                |
                      |               |                |
         +------------+         +------------+         +------------+
         |            |         |            |         |            |
   … … … |  Atom      |         |  Atom      |         |  Atom      |  … … …
         |            |         |            |         |            |
         +------------+       +->-----^------<-+       +------------+
                              |       |        |             
                              |       |        |             
                 +------------+       |        +------------+
                 |            |      symb      |            |
                 |  Nucleus   |         … … …  |    Prim    |  … … …
                 |            |                |            |
                 ^------------^                ^------^-----^
                 |            |               /       |      \                              
                 |            |              /        |       \                             
                 |            |             /    +----+----+   \                           
          ##position       charge        alpha   | Nucleus |  ##q_nums                           
                                                 +---------+
```
### How to create an input file?
Here is an example input for RHF/3-21G calculation on H2 molecule with bond length 1.4 bohr
```
# HF Primer 
# Input file

ATOMS 1 1
DISTANCE   1.4
BASIS     3-21G
# CBAS 0.1 0.2 0.3
```
   - anything beyond a '\#' is comment
   - bond length should be in bohr
   - only uncontracted type of basis set is supported
   - only 2-e systems are supported
   - CBAS option allows you to enter custom basis
