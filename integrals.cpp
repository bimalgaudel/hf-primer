#include <cmath>
#include "integrals.h"

double integral_overlap(PrimPair pair)
{
    return norm_const_stype(pair.prim_a.alpha)
            * norm_const_stype(pair.prim_b.alpha)
            * _integral_overlap_unormed(pair);
}

double integral_ke(PrimPair pair)
{
    // see question 4 in project.pdf
    double alpha_a = pair.prim_a.alpha;
    double alpha_b = pair.prim_b.alpha;
    double T_xyz = 0.0;
    for (int i = 0; i < DIMS; ++i) {
        // primitive prim_B has one component
        // of the angular momentum equal to 2
        Prim prim_B = pair.prim_b;
        prim_B.q_nums[i] = 2;
        PrimPair aB_pair = {pair.prim_a, prim_B};
        //
        double T_dim  = alpha_b*_integral_overlap_unormed(pair);
        T_dim        -= 2*std::pow(alpha_b, 2.0)
                         *_integral_overlap_unormed(aB_pair);
        T_xyz        += T_dim;
    }
    return norm_const_stype(alpha_a)*norm_const_stype(alpha_b)*T_xyz;
    // FOLLOWING CODE IS IMPLEMENTED ACCORDING TO
    // EQ. A.11 FROM SZABO, SEEMS BUGGY
    /* // see equation 11 of APpendix A in Szabo */
    /* // */
    /* double alpha_a = pair.prim_a.alpha; */
    /* double alpha_b = pair.prim_b.alpha; */
    /* // */
    /* pos_vec position_a = pair.prim_a.position; */
    /* pos_vec position_b = pair.prim_b.position; */
    /* // */
    /* double apb = alpha_a + alpha_b; */
    /* // */
    /* double atb = alpha_a * alpha_b; */
    /* // calculating (R_a - R_b)^2 */
    /* double Ra_Rb_sq = distance_sq(position_a, position_b); */
    /* // */
    /* double integral  = atb/apb*(3 - 2*atb/(apb)*Ra_Rb_sq); */
    /* integral       *= std::pow(M_PI/apb, 3.0/2.0); */
    /* integral       *= std::exp(-1*atb/apb * Ra_Rb_sq); */
    /* return integral; */
}

double integral_nuclear_att(Nuclei nuclei, PrimPair pair)
{
    // see equation 18, 19 and 20 from 'project.pdf'
    //
    double alpha_a = pair.prim_a.alpha;
    double alpha_b = pair.prim_b.alpha;
    double gamma = alpha_a + alpha_b;
    //
    pos_vec position_a = pair.prim_a.position;
    pos_vec position_b = pair.prim_b.position;
    //
    std::vector<double> P;
    for (int i = 0; i < DIMS; ++i) {
        P.push_back((alpha_a*position_a[i] + alpha_b*position_b[i])/gamma);
    }
    //
    double dist_AB_sq = distance_sq(position_a, position_b);
    //
    double integral = -1.0;
    integral *= norm_const_stype(alpha_a)*norm_const_stype(alpha_b);
    integral *= 2*M_PI*std::exp(-1*alpha_a*alpha_b*dist_AB_sq/gamma)/gamma;
    double sum = 0;
    for (Nuclei::iterator i = nuclei.begin(); i != nuclei.end(); ++i) {
        int charge = i->charge;
        double T = gamma * distance_sq(P, i->position);
        // if T > 0 then compute F_0 function (eq 20), otherwise it's value is 1
        double f_0 = (( T > 0.0))
                    ? 1/2.0*std::pow(M_PI/T, 1/2.0)*std::erf(std::pow(T, 1/2.0))
                    : 1;
        sum += charge*f_0;
    }
    integral *= sum;
    return integral;
}

double integral_electric_rep(PrimPair pair_x, PrimPair pair_y)
{
    Prim prim_a = pair_x.prim_a;
    Prim prim_b = pair_x.prim_b;
    Prim prim_c = pair_y.prim_a;
    Prim prim_d = pair_y.prim_b;
    //
    double gamma_p = prim_a.alpha + prim_b.alpha;
    double gamma_q = prim_c.alpha + prim_d.alpha;
    //
    pos_vec P, Q;
    for (int i = 0; i < DIMS; ++i) {
        P.push_back((prim_a.alpha*prim_a.position[i]
                    + prim_b.alpha*prim_b.position[i])/gamma_p);
        //
        Q.push_back((prim_c.alpha*prim_c.position[i]
                    + prim_d.alpha*prim_d.position[i])/gamma_q);
    }
    //
    double K_ab = std::exp(-1*prim_a.alpha*prim_b.alpha*distance_sq(prim_a.position,
                prim_b.position)/gamma_p);
    double K_cd = std::exp(-1*prim_c.alpha*prim_d.alpha*distance_sq(prim_c.position,
                prim_d.position)/gamma_q);
    //
    double integral = norm_const_stype(prim_a.alpha)
                        * norm_const_stype(prim_b.alpha) 
                        * norm_const_stype(prim_c.alpha)
                        * norm_const_stype(prim_d.alpha);
    integral *= 2*std::pow(M_PI, 5.0/2) * K_ab * K_cd
                    / (gamma_p*gamma_q*std::pow(gamma_p + gamma_q, 1.0/2));
    double T = (distance_sq(P, Q) * gamma_p * gamma_q/(gamma_p + gamma_q));
    // if T > 0 then compute F_0 function (eq 20), otherwise it's value is 1
    double f_0 = (( T > 0.0))
        ? 1/2.0*std::pow(M_PI/T, 1/2.0)*std::erf(std::pow(T, 1/2.0))
        : 1;
    integral *= f_0;
    return integral;
}

//****************************************************

double _integral_overlap_unormed(PrimPair pair)
{
    double integral = 1.0;
    for (int i = 0; i < DIMS; ++i)
    {
        integral *= _integral_overlap_1d(pair, i);
    }
    return integral;
}

double _integral_overlap_1d(PrimPair pair, int dim)
{
    // calculating overlap integral between
    //  two one-dimensional gaussian primitives
    //  which dimension to integrate of a pair of
    //  primitives is selected by dim
    //  dim can have values 0, 1, 2
    double alpha_a = pair.prim_a.alpha;
    double alpha_b = pair.prim_b.alpha;
     
    int qnum_a = pair.prim_a.q_nums[dim];
    int qnum_b = pair.prim_b.q_nums[dim];

    double vec_component_a = pair.prim_a.position[dim];
    double vec_component_b = pair.prim_b.position[dim];
     
    double gamma = alpha_a + alpha_b;
    double P     = (alpha_a
                   * vec_component_a
                   + alpha_b
                   * vec_component_b) / gamma;
    // CAUTION: Hard coded numbers
    // see 'project.pdf'
    // calculating according to the equation(7) of the project
    double integral = 1.0;
    integral *= std::exp(-1 * alpha_a*alpha_b
                            * std::pow((vec_component_a
                                         - vec_component_b), 2) / gamma);
    //
    double sum = 0;
    // the upper limit of 'i' in the loop is rounded
    //  down by the division, which is desired
    for (int i = 0; i < (qnum_a + qnum_b)/2+1; ++i) 
    {
        double calc = factorial(factorial(2*i - 1))/std::pow(2.0*gamma, i)
                      * std::pow(M_PI/gamma, 1.0/2);
             calc *= _angular_momentum_function(2*i, qnum_a, qnum_b,
                                                P - vec_component_a,
                                                P - vec_component_b);
              sum += calc;
    }
    // forgot to sum*integral :D
    return sum*integral;
}

double _angular_momentum_function(int index,
                                int l_1, int l_2,
                                double L_1, double L_2)
{
    // equation 10 from 'project.pdf'
    //
    // limits of the summation range
    // using syntactic sugar
    int lower_limit = ((0 > (index - l_2)) ? 0 : index - l_2);
    int upper_limit = ((l_1 < index) ? l_1 : index);
    //
    double sum  = 0;
    for (int i = lower_limit; i < upper_limit+1; ++i)
    {
        double product  = std::pow(L_1, l_1-i) * std::pow(L_2, l_2-index+i);
        product       *= combination(l_1, i) * combination(l_2, index -i );
        sum           += product;
    }
    return sum;
}

//*****************************
// smaller helper functions
// ****************************

double norm_const_stype(double orb_exponent)
{
    // Normalization constant( N(alpha) ) for primitive
    // spherical Gaussian is:
    // N(alpha) = (2*alpha/Pi)^(3/4)
    //
    return std::pow(2*orb_exponent/M_PI, 3.0/4.0);
}

int factorial(int n, int stop)
{
    // calculates factorial of an integer
    //  int stop = 1, by default, is when 
    //  the true factorial is calculated
    //  returns: (n!)/(n - stop)! = n*(n-1)*(n-2)*...*(n-stop+1)
    int fact = 1;
    if (n < 1) { return fact; }
    while (n > stop)
    {
        fact *= n;
        --n;
    }
    return fact;
    // the following commented method
    // that uses recursion gave runtime error
    // segmentation fault; although it works
    // in relatively small programs
/* { */
/*     if (n < stop+1) { */
/*         return 1; */
/*     } else { */
/*         return n * factorial(n-1,stop); */
/*     } */
/* } */
}

int combination(int a, int b)
{
    // Calculates
    // a       a!
    //  C = -------
    // b    b!(a-b)!
    return factorial(a, (a-b))/factorial(b);
}

double distance_sq(pos_vec pos_a, pos_vec pos_b)
{
    // calculates the distance between two points
    // in R^DIMS coordinate system
    double distance = 0;
    for (int i = 0; i < DIMS; ++i) {
        // the distance formula
        distance += pow(pos_a[i] - pos_b[i], 2);
    }
    return distance;
}

double energy_nuclear(Molecule molecule)
{
    double energy = 0;
    Nuclei nuclei = molecule.nuclei;
    int num_nuclei = nuclei.size();
    for (int i = 0; i < num_nuclei-1; ++i) {
        for (int j = i+1; j < num_nuclei; ++j) {
            double distance = distance_sq(nuclei[i].position, nuclei[j].position);
            distance = std::pow(distance, 1.0/2);
            energy += (nuclei[i].charge * nuclei[j].charge) / distance;
        }
    }
    return energy;
}
